import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgProgress } from '@ngx-progressbar/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: any;
  password: any;
  constructor(
    private auth: AuthenticationService, private router: Router,
    private toastr: ToastrService, private progress: NgProgress) { }

  ngOnInit() {

  }

  signIn() {
    const val = this.auth.signIn(this.username, this.password).subscribe((data: any) => {
      if (data === false) {
        this.toastr.warning('Please check your Username / Password and try again');
      } else {
        localStorage.setItem('token', data.token);
        localStorage.setItem('user', this.username);
        this.router.navigate(['vehicle']);
      }
    });

  }
}
