import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineItemTypeComponent } from './define-item-type.component';

describe('DefineItemTypeComponent', () => {
  let component: DefineItemTypeComponent;
  let fixture: ComponentFixture<DefineItemTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineItemTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineItemTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
