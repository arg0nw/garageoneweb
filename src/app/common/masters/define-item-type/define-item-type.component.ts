import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { MastersService } from '../masters.service';
import { ToastrService } from 'ngx-toastr';
import { AddNewItemTypesPopupComponent } from './add-new-item-types-popup/add-new-item-types-popup.component';
import { ItemTpye } from 'src/app/models/ItemType/ItemType';

@Component({
  selector: 'app-define-item-type',
  templateUrl: './define-item-type.component.html',
  styleUrls: ['./define-item-type.component.css']
})
export class DefineItemTypeComponent implements OnInit {

  itemType = {} as ItemTpye;
  itemTypes = [];
  displayedColumns: string[] = [
    'itemType',
    'itemTypeDescription',
    'action'
  ];

  dataSource = new MatTableDataSource(this.itemTypes);
  isBusy = false;

  constructor(
    public dialog: MatDialog,
    private service: MastersService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getAllItemTypes();
  }

  addNewItemType() {
    const dialogRef = this.dialog.open(AddNewItemTypesPopupComponent, {
      width: '500px',
      data: this.itemType
    });
  }

  getAllItemTypes() {
    this.isBusy = true;
    this.service.getAllItemTypes().subscribe((data: any) => {
      if (data.Code === 0) {
        this.dataSource = this.itemTypes = data.Result;
        this.toastr.success('Data fetched successfully!');
        console.log(this.itemTypes);
        this.isBusy = false;
      } else {
        this.toastr.error(data.Message);
        this.isBusy = false;
      }
    }, error => {
      this.toastr.warning(error);
      this.isBusy = false;
    });
  }

  updateService(itemT) {
    const dialogRef = this.dialog.open(AddNewItemTypesPopupComponent, {
      width: '500px',
      data: itemT
    });
  }

  deleteSerive(itemT) {
    // tslint:disable-next-line:max-line-length
    const conf = confirm('If you delete this record, the reference will be deleted from here onwards. You may not be able to see the item type in the inventory. Still Do you need to delete this item?');
    if (conf) {
      this.isBusy = true;
      this.service.deleteItemType(itemT).subscribe((data: any) => {
        if (data.Code === 0) {
          this.toastr.success(data.Message);
          this.getAllItemTypes();
        } else {
          this.toastr.error(data.Message);
          this.isBusy = false;
        }
      }, error => {
        this.toastr.warning(error);
        this.isBusy = false;
      });
    }
  }
}
