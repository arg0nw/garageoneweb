import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddNewServiceTypeComponent } from '../../define-service/add-new-service-type/add-new-service-type.component';
import { MastersService } from '../../masters.service';
import { ToastrService } from 'ngx-toastr';
import { ServiceType } from 'src/app/models/ServiceType/serviceType';
import { ItemTpye } from 'src/app/models/ItemType/ItemType';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-new-item-types-popup',
  templateUrl: './add-new-item-types-popup.component.html',
  styleUrls: ['./add-new-item-types-popup.component.css']
})
export class AddNewItemTypesPopupComponent implements OnInit {

  itemType = {} as ItemTpye;

  constructor(
    public dialogRef: MatDialogRef<AddNewServiceTypeComponent>,
    private service: MastersService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: ItemTpye
  ) {
    this.itemType = data;
  }

  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  postItemType(form: NgForm) {
    if (form.valid) {
      if (!this.itemType.id) {
        this.service.postNewItemType(this.itemType).subscribe((data: any) => {
          if (data.Code !== 0) {
            this.toastr.error(data.Message);
          } else {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          }
        }, error => {
          this.toastr.warning(error);
        });
      } else {
        this.service.updateItemType(this.itemType).subscribe((data: any) => {
          if (data.Code !== 0) {
            this.toastr.error(data.Message);
          } else {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          }
        }, error => {
          this.toastr.warning(error);
        });
      }
    } else {
      this.toastr.warning('Please fill the mandetory fields!');
    }
  }

}

