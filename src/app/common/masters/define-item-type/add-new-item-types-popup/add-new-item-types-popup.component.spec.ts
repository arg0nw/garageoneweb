import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewItemTypesPopupComponent } from './add-new-item-types-popup.component';

describe('AddNewItemTypesPopupComponent', () => {
  let component: AddNewItemTypesPopupComponent;
  let fixture: ComponentFixture<AddNewItemTypesPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewItemTypesPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewItemTypesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
