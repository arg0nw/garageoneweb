import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AddNewServiceTypeComponent } from './add-new-service-type/add-new-service-type.component';
import { MastersService } from '../masters.service';
import { Response } from 'src/app/models/Response';
import { ToastrService } from 'ngx-toastr';
import { ServiceType } from 'src/app/models/ServiceType/serviceType';

@Component({
  selector: 'app-define-service',
  templateUrl: './define-service.component.html',
  styleUrls: ['./define-service.component.css']
})
export class DefineServiceComponent implements OnInit {

  services = [];
  displayedColumns: string[] = [
    'serviceName',
    'unitPrice',
    'serviceType',
    'action'
  ];

  dataSource = new MatTableDataSource(this.services);

  serviceType = {} as ServiceType;
  isBusy = false;

  constructor(
    public dialog: MatDialog,
    private service: MastersService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getAllServiceAndRepair();
  }

  addNewService() {
    const dialogRef = this.dialog.open(AddNewServiceTypeComponent, {
      width: '50%',
      data: this.serviceType
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllServiceAndRepair() {
    this.isBusy = true;
    this.service.getAllServiceRepairType().subscribe((data: Response) => {
      if (data.Code === 0) {
        this.dataSource = data.Result;
        this.toastr.success('Data fetched successfully!');
        this.isBusy = false;
      } else {
        this.toastr.error(data.Message);
        this.isBusy = false;
      }
    });
  }

  updateService(service) {
    const dialogRef = this.dialog.open(AddNewServiceTypeComponent, {
      width: '50%',
      data: service
    });
  }

  deleteSerive(service) {
    // tslint:disable-next-line:max-line-length
    const conf = confirm('If you delete this record, the reference will be deleted from here onwards. You may not be able to see the item type in the inventory. Still Do you need to delete this item?');
    if (conf) {
      this.isBusy = true;
      this.service.deleteServiceRepairType(service).subscribe((data: Response) => {
        if (data.Code === 0) {
          this.toastr.success(data.Message);
          this.getAllServiceAndRepair();
        } else {
          this.toastr.error(data.Message);
          this.isBusy = false;
        }
      });
    }
  }
}
