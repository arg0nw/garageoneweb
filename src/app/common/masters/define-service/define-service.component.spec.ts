import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineServiceComponent } from './define-service.component';

describe('DefineServiceComponent', () => {
  let component: DefineServiceComponent;
  let fixture: ComponentFixture<DefineServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
