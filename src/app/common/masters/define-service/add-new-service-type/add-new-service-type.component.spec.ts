import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewServiceTypeComponent } from './add-new-service-type.component';

describe('AddNewServiceTypeComponent', () => {
  let component: AddNewServiceTypeComponent;
  let fixture: ComponentFixture<AddNewServiceTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewServiceTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewServiceTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
