import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ServiceType } from 'src/app/models/ServiceType/serviceType';
import { NgForm } from '@angular/forms';
import { MastersService } from '../../masters.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-new-service-type',
  templateUrl: './add-new-service-type.component.html',
  styleUrls: ['./add-new-service-type.component.css'],
  providers: [MastersService]
})
export class AddNewServiceTypeComponent implements OnInit {

  newService = {} as ServiceType;

  constructor(
    public dialogRef: MatDialogRef<AddNewServiceTypeComponent>,
    private service: MastersService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: ServiceType
  ) {
    this.newService = data;
  }

  types: any[] = [
    { value: 0, viewValue: 'Service' },
    { value: 1, viewValue: 'Repair' },
  ];

  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }

  saveSerive(form: NgForm) {
    if (form.valid) {
      if (!this.newService.id) {
        this.service.postNewServiceType(this.newService).subscribe((data: any) => {
          if (data.Code !== 0) {
            this.toastr.error(data.Message);
          } else {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          }
        }, error => {
          this.toastr.warning(error);
        });
      } else {
        this.service.updateServiceType(this.newService).subscribe((data: any) => {
          if (data.Code !== 0) {
            this.toastr.error(data.Message);
          } else {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          }
        }, error => {
          this.toastr.warning(error);
        });
      }
    } else {
      this.toastr.warning('Please fill the mandetory fileds!');
    }
  }
}
