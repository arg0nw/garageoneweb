import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineRepairComponent } from './define-repair.component';

describe('DefineRepairComponent', () => {
  let component: DefineRepairComponent;
  let fixture: ComponentFixture<DefineRepairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineRepairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineRepairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
