import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HOME_URL } from 'src/app/_helper/appConstant';
import { Response } from 'src/app/models/Response';

@Injectable({
  providedIn: 'root'
})
export class MastersService {

  res: Observable<Response>;
  constructor(private http: HttpClient) { }

  // Service Type Master Related APIs
  postNewServiceType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/serviceType/postnewservicetype', obj);
  }

  updateServiceType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/serviceType/updateservicetype', obj);
  }

  getAllServiceRepairType() {
    return this.http.get<Response>(HOME_URL + 'masters/serviceType/getallservices');
  }

  deleteServiceRepairType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/serviceType/deleteservicetype', obj);
  }

  // Item Type Master Related APIs
  postNewItemType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/itemType/addnewitemtype', obj);
  }

  updateItemType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/itemType/updateitemtype', obj);
  }

  getAllItemTypes() {
    return this.http.get<Response>(HOME_URL + 'masters/itemType/getallitemtypes');
  }

  deleteItemType(obj) {
    return this.http.post<Response>(HOME_URL + 'masters/itemType/deleteitemtype', obj);
  }
}
