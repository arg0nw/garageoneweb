import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootbuttonsComponent } from './rootbuttons.component';

describe('RootbuttonsComponent', () => {
  let component: RootbuttonsComponent;
  let fixture: ComponentFixture<RootbuttonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootbuttonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootbuttonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
