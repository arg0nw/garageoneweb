import { Component, OnInit } from '@angular/core';
import { Vehicle } from 'src/app/models/Vehicle/vehicle';
import { RegisterNewVehicleComponent } from 'src/app/basic/vehicle-management/register-new-vehicle/register-new-vehicle.component';
import { ReportIssueComponent } from 'src/app/basic/vehicle-management/report-issue/report-issue.component';
import { MatDialog } from '@angular/material';
import { InventoryItem } from 'src/app/models/Inventory/inventoryItem';
import { AddNewItemComponent } from 'src/app/basic/inventory/add-new-item/add-new-item.component';
import { MessagepopComponent } from '../messagepop/messagepop.component';

@Component({
  selector: 'app-rootbuttons',
  templateUrl: './rootbuttons.component.html',
  styleUrls: ['./rootbuttons.component.css']
})
export class RootbuttonsComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openRegisterVehicle() {
    const vehicle = {} as Vehicle;
    const dialogRef = this.dialog.open(RegisterNewVehicleComponent, {
      width: '50%',
      data: { data: vehicle, isUpdate: false }
    });
  }

  openReportIssue() {
    const dialogRef = this.dialog.open(ReportIssueComponent, {
      width: '50%',
      // data: this.vehicles
    });
  }

  openAddInventoryItem() {
    const item = {} as InventoryItem;
    const dialogRef = this.dialog.open(AddNewItemComponent, {
      width: '50%',
      data: { data: item }
    });
  }

  usageUpdate() {
    const dialogRef = this.dialog.open(MessagepopComponent, {
      width: '30%',
      data: { message: 'This feature will be available soon!' }
    });
  }

}
