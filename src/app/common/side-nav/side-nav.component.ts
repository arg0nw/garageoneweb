import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  menuItems = [
    {
      moduleName: 'Vehicle Management', icon: 'directions_car', opened: 'true', moduleItems:
        [
          {
            name: 'View all', path: '/vehicle',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Inventory Management', icon: 'settings_input_composite', opened: 'false', moduleItems:
        [
          {
            name: 'View all', path: '/inventory',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Job / Est: Management', icon: 'check_circle', opened: 'false', moduleItems:
        [
          {
            name: 'Add Job / Est: ', path: '/newtask',
            // permissionCode: 1
          },
          {
            name: 'View all jobs / esti:', path: '/task',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Invoice & Billing', icon: 'attach_money', opened: 'false', moduleItems:
        [
          {
            name: 'Get Prints', path: '/finance',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Payroll Management', icon: 'credit_card', opened: 'false', moduleItems:
        [
          {
            name: 'View all', path: '/payroll',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Masters', icon: 'view_comfy', opened: 'false', moduleItems:
        [
          {
            name: 'Define service', path: '/defservice',
            // permissionCode: 1
          },
          {
            name: 'Define item type', path: '/defitem',
            // permissionCode: 1
          }
        ]
    },
    {
      moduleName: 'Reports', icon: 'receipt', opened: 'false', moduleItems:
        [
          {
            name: 'View all', path: '/reports',
            // permissionCode: 1
          }
        ]
    },

  ];

  constructor() { }

  ngOnInit() {
  }

}
