import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagepopComponent } from './messagepop.component';

describe('MessagepopComponent', () => {
  let component: MessagepopComponent;
  let fixture: ComponentFixture<MessagepopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagepopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagepopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
