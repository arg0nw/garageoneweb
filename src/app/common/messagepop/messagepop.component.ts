import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-messagepop',
  templateUrl: './messagepop.component.html',
  styleUrls: ['./messagepop.component.css']
})
export class MessagepopComponent implements OnInit {
  Message: any;
  constructor(
    public dialogRef: MatDialogRef<MessagePortEventMap>,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.Message = data.message;
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
