import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedUser: string;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {
    this.loggedUser = localStorage.getItem('user');
  }

  signOut() {
    this.auth.signOut();
  }
}
