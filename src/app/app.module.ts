import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './common/home/home.component';
import { SideNavComponent } from './common/side-nav/side-nav.component';
import { HeaderComponent } from './common/header/header.component';
import { InventoryComponent } from './basic/inventory/inventory.component';
import { JobManagementComponent } from './basic/job-management/job-management.component';
import { PayrollComponent } from './pro/payroll/payroll.component';
import { FinancialComponent } from './pro/financial/financial.component';
import { LoginComponent } from './common/login/login.component';
import { VehicleManagementComponent } from './basic/vehicle-management/vehicle-management.component';
import { GeneralReportComponent } from './pro/report/general-report/general-report.component';
import { AuthGuard } from './auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatDividerModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatToolbarModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule,
  // tslint:disable-next-line:max-line-length
  MatChipsModule,
  MatAutocompleteModule,
  MatListModule,
  MatIconModule,
  MatFormFieldModule,
  MatTabsModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatRadioModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AddNewItemComponent } from './basic/inventory/add-new-item/add-new-item.component';
import { RegisterNewVehicleComponent } from './basic/vehicle-management/register-new-vehicle/register-new-vehicle.component';
import { AddNewJobComponent } from './basic/job-management/add-new-job/add-new-job.component';
import { UpdateJobComponent } from './basic/job-management/update-job/update-job.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReportIssueComponent } from './basic/vehicle-management/report-issue/report-issue.component';
// import { NgProgressModule } from '@ngx-progressbar/core';
import { ToastrModule } from 'ngx-toastr';
import { MessagepopComponent } from './common/messagepop/messagepop.component';
import { RootbuttonsComponent } from './common/rootbuttons/rootbuttons.component';
import { DefineServiceComponent } from './common/masters/define-service/define-service.component';
import { DefineRepairComponent } from './common/masters/define-repair/define-repair.component';
import { AddNewServiceTypeComponent } from './common/masters/define-service/add-new-service-type/add-new-service-type.component';
import { DefineItemTypeComponent } from './common/masters/define-item-type/define-item-type.component';
// tslint:disable-next-line:max-line-length
import { AddNewItemTypesPopupComponent } from './common/masters/define-item-type/add-new-item-types-popup/add-new-item-types-popup.component';
import { MatPaginatorModule } from '@angular/material';
import { SearchJobComponent } from './basic/job-management/search-job/search-job.component';
import { CustomerManagementComponent } from './basic/customer-management/customer-management.component';
import { ServiceInterceptor } from './services/service.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideNavComponent,
    HeaderComponent,
    InventoryComponent,
    JobManagementComponent,
    PayrollComponent,
    FinancialComponent,
    LoginComponent,
    VehicleManagementComponent,
    GeneralReportComponent,
    AddNewItemComponent,
    AddNewJobComponent,
    UpdateJobComponent,
    RegisterNewVehicleComponent,
    ReportIssueComponent,
    MessagepopComponent,
    RootbuttonsComponent,
    DefineServiceComponent,

    DefineRepairComponent,
    AddNewServiceTypeComponent,
    DefineItemTypeComponent,
    AddNewItemTypesPopupComponent,
    SearchJobComponent,
    CustomerManagementComponent
  ],
  entryComponents: [
    RegisterNewVehicleComponent,
    ReportIssueComponent,
    AddNewItemComponent,
    MessagepopComponent,
    AddNewServiceTypeComponent,
    AddNewItemTypesPopupComponent,
    SearchJobComponent,
    UpdateJobComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    // NgProgressModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatToolbarModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatDividerModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatIconModule,
    MatChipsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: ServiceInterceptor, multi: true },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
