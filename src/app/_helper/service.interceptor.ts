import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

export class ServiceInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {

        const newRequest = req.clone({
            headers: req.headers.set(
                'Content-Type',
                'application/json'
            )
        });

        // console.log(newRequest);
        return next.handle(newRequest);
    }
}
