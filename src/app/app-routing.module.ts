import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './common/home/home.component';
import { AuthGuard } from './auth.guard';
import { VehicleManagementComponent } from './basic/vehicle-management/vehicle-management.component';
import { LoginComponent } from './common/login/login.component';
import { InventoryComponent } from './basic/inventory/inventory.component';
import { JobManagementComponent } from './basic/job-management/job-management.component';
import { FinancialComponent } from './pro/financial/financial.component';
import { PayrollComponent } from './pro/payroll/payroll.component';
import { ReportIssueComponent } from './basic/vehicle-management/report-issue/report-issue.component';
import { GeneralReportComponent } from './pro/report/general-report/general-report.component';
import { DefineServiceComponent } from './common/masters/define-service/define-service.component';
import { DefineRepairComponent } from './common/masters/define-repair/define-repair.component';
import { DefineItemTypeComponent } from './common/masters/define-item-type/define-item-type.component';
import { AddNewJobComponent } from './basic/job-management/add-new-job/add-new-job.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children:
      [
        {
          path: 'vehicle',
          component: VehicleManagementComponent
        },
        {
          path: '',
          redirectTo: '/vehicle',
          pathMatch: 'full'
        },
        {
          path: 'inventory',
          component: InventoryComponent
        },
        {
          path: '',
          redirectTo: '/inventory',
          pathMatch: 'full'
        },
        {
          path: 'task',
          component: JobManagementComponent
        },
        {
          path: '',
          redirectTo: '/task',
          pathMatch: 'full'
        },
        {
          path: 'newtask',
          component: AddNewJobComponent
        },
        {
          path: '',
          redirectTo: '/newtask',
          pathMatch: 'full'
        },
        {
          path: 'finance',
          component: FinancialComponent
        },
        {
          path: '',
          redirectTo: '/finance',
          pathMatch: 'full'
        },
        {
          path: 'payroll',
          component: PayrollComponent
        },
        {
          path: '',
          redirectTo: '/payroll',
          pathMatch: 'full'
        },
        {
          path: 'reports',
          component: GeneralReportComponent
        },
        {
          path: '',
          redirectTo: '/reports',
          pathMatch: 'full'
        },
        {
          path: 'defservice',
          component: DefineServiceComponent
        },
        {
          path: '',
          redirectTo: '/defservice',
          pathMatch: 'full'
        },
        {
          path: 'defitem',
          component: DefineItemTypeComponent
        },
        {
          path: '',
          redirectTo: '/defitem',
          pathMatch: 'full'
        }
      ],
  },
  {
    path: 'auth',
    component: LoginComponent,
    // children: [
    //   {
    //     path: 'auth',
    //     component: LoginComponent,
    //   }
    // ]
    // canActivate: [AuthGuard],
    // children: [
    //   {
    //     path : 'statistic',
    //     component : StatisticComponent
    //   },
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'ignore' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
