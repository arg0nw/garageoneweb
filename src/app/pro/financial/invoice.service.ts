import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HOME_URL } from 'src/app/_helper/appConstant';
import { Response } from 'src/app/models/Response';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private http: HttpClient) { }

  generateInvoiceNumber(obj) {
    return this.http.post<Response>(HOME_URL + 'invoice/createnew', obj);
  }
}
