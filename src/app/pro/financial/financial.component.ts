import { Component, OnInit } from '@angular/core';
import { BilligServiceService } from 'src/app/services/billig-service.service';
import { DatePipe } from '@angular/common';
import { InvoiceService } from './invoice.service';

@Component({
  selector: 'app-financial',
  templateUrl: './financial.component.html',
  styleUrls: ['./financial.component.css']
})
export class FinancialComponent implements OnInit {

  isBusy = false;

  Invoice = {} as any;
  Status: any;
  jobId: any;
  constructor(
    private service: BilligServiceService,
    private datePipe: DatePipe,
    private invoService: InvoiceService
  ) {
    this.Invoice.customer = {} as any;
    this.Invoice.mainData = {} as any;
    this.Invoice.vehicle = {} as any;
  }

  ngOnInit() {
  }

  searchJobById() {
    this.isBusy = true;
    if (this.jobId) {
      this.service.getJobdataById(this.jobId).subscribe((res: any) => {
        console.log(res);
        res.Result.mainData.createdAt = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.Invoice = res.Result;
        this.isBusy = false;
        this.Status = res.Result.mainData.taskStatus === 1 ? 'Pending' :
          res.Result.mainData.taskStatus === 2 ? 'In-Progress' :
            res.Result.mainData.taskStatus === 3 ? 'Ready to review' :
              res.Result.mainData.taskStatus === 4 ? 'Done' : '';
      }, err => {
      });
      this.isBusy = false;
    }
  }

  calculateTotal() {
    this.Invoice.mainData.totalInvoiceAmount =
      Number(this.Invoice.mainData.taskEstimatedPrice) - (
        Number(this.Invoice.mainData.taskEstimatedPrice) * Number(this.Invoice.mainData.discount) / 100
      );
  }

  printInvoice() {

    this.isBusy = true;
    this.Invoice.mainData.printType = !this.Invoice.mainData.isEstimation ? 'Sales Invoice' : 'Estimation';
    this.Invoice.mainData.InvoiceOrEstimationNo = !this.Invoice.mainData.isEstimation ? 'Invoice No' : 'Estimation No';
    this.Invoice.mainData.taskServiceCharge = (Number(!this.Invoice.mainData.taskServiceCharge ? 0.00 :
      this.Invoice.mainData.taskServiceCharge).toFixed(2));
    this.Invoice.mainData.totalInvoiceAmount = !this.Invoice.mainData.totalInvoiceAmount ?
      this.Invoice.mainData.taskEstimatedPrice :
      Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
    this.Invoice.mainData.discount = !this.Invoice.mainData.discount ? '0' : Number(this.Invoice.mainData.discount);


    this.Invoice.mainData.totalInvoiceAmount = Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
    this.Invoice.mainData.taskEstimatedPrice = Number(this.Invoice.mainData.taskEstimatedPrice).toFixed(2);

    const obj = {
      discountAmount: this.Invoice.mainData.discount,
      invoiceTotal: this.Invoice.mainData.totalInvoiceAmount,
      meterReading: this.Invoice.vehicle.meterReading,
      nextService: this.Invoice.vehicle.nextService,
      serviceSpecificTaskId: this.Invoice.mainData.id
    };

    if (this.Invoice.mainData.isEstimation) {
      this.Invoice.mainData.invoiceNumber = this.Invoice.mainData.taskId;
      this.service.getAllJobsReport(this.Invoice);
      this.isBusy = false;
    } else {
      this.invoService.generateInvoiceNumber(obj).subscribe(data => {
        console.log(data);
        this.Invoice.mainData.invoiceNumber = data.Result.invoiceNumber;
        this.service.getAllJobsReport(this.Invoice);
        this.isBusy = false;
      });
    }
  }

}
