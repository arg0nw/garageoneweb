import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { sha256 } from 'js-sha256';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NgProgress } from '@ngx-progressbar/core';
import { HOME_URL } from '../_helper/appConstant';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router) {

  }

  signIn(nic, password) {

    const url = HOME_URL + 'signin';

    const NetworkStatus1 = btoa(nic);
    const NetworkStatus2 = btoa(sha256(password));

    // // // console.log('Net stat 2 :' + networkStatus2);

    const body = {
      networkStatus1: NetworkStatus1,
      networkStatus2: NetworkStatus2
    };

    return this.http.post(url, body, httpOptions);
  }

  signOut() {
    localStorage.clear();
    this.router.navigate(['auth']);
  }
}
