declare var require: any;
import { Injectable } from '@angular/core';
import { JobServiceService } from '../basic/job-management/job-service.service';
import { DatePipe, JsonPipe } from '@angular/common';
import { REPORT_SERVER } from '../_helper/appConstant';

const jsreport = require('jsreport-browser-client-dist');

@Injectable({
  providedIn: 'root'
})
export class BilligServiceService {
  constructor(
    private jobserviceservice: JobServiceService,
    private datePipe: DatePipe
  ) { }

  jobData: any = {};

  getJobdataById(id) {
    return this.jobserviceservice.getJobDataById(id);
  }

  getAllJobsReport(jobs) {
    jsreport.serverUrl = REPORT_SERVER;
    const data = {
      template: { shortid: 'r1qao9KnE' },
      data: jobs
    };
    console.log(data);
    // jsreport.download(jobs.mainData.invoiceNumber + '.pdf', data);

    // tslint:disable-next-line:only-arrow-functions
    jsreport.renderAsync(data).then(function(res) {
      const html = '<html>' +
        '<style>html,body {padding:0;margin:0;} iframe {width:100%;height:100%;border:0}</style>' +
        '<body>' +
        '<iframe type="application/pdf" src="' + res.toDataURI() + '"></iframe>' +
        '</body></html>';
      const a = window.open('about:blank', 'Report');
      a.document.write(html);
      a.document.close();
    });
  }
}
