export class ServiceType {
    id?: number;
    serviceName: string;
    serviceType: number;
    unitPrice: number;
    description: string;
}
