export class VehicleIssue {
    vehiclePlateNumber: string;
    reportedDate: Date;
    issueDescription: string;
    addedUser: string;
    reportedTechnician: string;
}
