export class Vehicle {
    id: number;
    plateNumber: string;
    customerName: string;
    contactNumber: string;
    vehicleModel: string;
    serviceType: string;
    vehicleRegisteredYear: number;
    vehicleRemark: string;
    addedDate: string;
    vehicleColor: string;
}
