export class InventoryItem {
    id?: any;
    itemCode: string;
    itemName: string;
    itemQuantity: string;
    isItemOutSource: string;
    itemRemark: string;
    itemSellingPrice: string;
    inventoryItemTypeId: number;
    itemUnitPrice: number;
}
