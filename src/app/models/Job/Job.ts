export class Job {
    id?: any;
    taskId: string;
    taskSubject: string;
    vehiclePlateNumber: string;
    taskDescription: string;
    taskEstimatedPrice: string;
    taskItemsTotalAmount: string;
    taskServiceCharge: string;
    taskEstimatedDurationInDays: number;
    taskAssignTo: number;
    taskStatus: any;
    itemList: any[];
    serviceTypeId: any;
    isEstimation: boolean;
}
