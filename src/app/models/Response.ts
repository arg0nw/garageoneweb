export class Response {
    Code: number;
    Message: string;
    Result: any;
}
