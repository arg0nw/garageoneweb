export class Customer {
    id?: any;
    customerName: string;
    contactNumber: string;
    secondaryContactNumber: string;
    drivingLicenseNumber: string;
    nicNumber: string;
    address: string;
    emailAddress: string;
}
