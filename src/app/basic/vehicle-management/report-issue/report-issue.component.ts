import { Component, OnInit, Inject } from '@angular/core';
import { VehicleIssue } from 'src/app/models/VehicleIssue/vehicleIssue';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VehicleManagementService } from '../vehicle-management.service';
import { Response } from 'src/app/models/Response';
import { Vehicle } from 'src/app/models/Vehicle/vehicle';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-report-issue',
  templateUrl: './report-issue.component.html',
  styleUrls: ['./report-issue.component.css']
})
export class ReportIssueComponent implements OnInit {

  issue = {} as VehicleIssue;
  options = [];
  constructor(
    public dialogRef: MatDialogRef<ReportIssueComponent>,
    private service: VehicleManagementService,
    @Inject(MAT_DIALOG_DATA) public data: Vehicle[],
    private toastr: ToastrService
  ) {
    if (data) {
      data.forEach(element => {
        this.options.push(element.plateNumber);
      });
    }

  }

  ngOnInit() {
    console.log(this.options);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitIssue(form: NgForm) {
    if (form.valid) {
      this.service.reportIssue(this.issue).subscribe((data: Response) => {
        if (data.Code !== 0) {
          this.toastr.success(data.Message);
          this.dialogRef.close();
        }
      });
    } else {
      this.toastr.warning('Please fill the mandetory fields!');
    }
  }

}
