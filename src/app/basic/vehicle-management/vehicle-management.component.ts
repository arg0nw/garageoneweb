import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { RegisterNewVehicleComponent } from './register-new-vehicle/register-new-vehicle.component';
import { VehicleManagementService } from './vehicle-management.service';
import { Response } from 'src/app/models/Response';
import { ReportIssueComponent } from './report-issue/report-issue.component';
// import { NgProgress } from '@ngx-progressbar/core';
import { ToastrService } from 'ngx-toastr';
import { Vehicle } from 'src/app/models/Vehicle/vehicle';
import { MessagepopComponent } from 'src/app/common/messagepop/messagepop.component';
import { Customer } from 'src/app/models/Customer/Customer';

@Component({
  selector: 'app-vehicle-management',
  templateUrl: './vehicle-management.component.html',
  styleUrls: ['./vehicle-management.component.css']
})
export class VehicleManagementComponent implements OnInit {

  isBusy = false;

  vehicles = [];
  dataSource = new MatTableDataSource(this.vehicles);

  displayedColumns: string[] = [
    'plateNumber',
    'customerName',
    'contactNumber',
    'vehicleModel',
    'action'
  ];

  constructor(
    public dialog: MatDialog,
    private service: VehicleManagementService,
    // private progress: NgProgress,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getAllRegisteredVehicle();
  }

  openRegisterVehicle() {
    const obj = {
      // tslint:disable-next-line:new-parens
      vehicle: new Vehicle,
      // tslint:disable-next-line:new-parens
      customer: new Customer
    };
    const dialogRef = this.dialog.open(RegisterNewVehicleComponent, {
      width: '50%',
      data: { data: obj, isUpdate: false }
    });
  }

  viewReportedIssue() {
    const dialogRef = this.dialog.open(MessagepopComponent, {
      width: '30%',
      data: { message: 'This feature will be available soon!' }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllRegisteredVehicle() {
    this.isBusy = true;
    // this.progress.start();
    this.service.getAllVehiclesDetails().subscribe((data: any) => {
      console.log(data);
      if (data.Code !== 0) {
        alert(data.Message.name);
        this.isBusy = false;

      } else {
        this.dataSource = this.vehicles = data.Result;
        // this.vehicles = data.Result;
        // this.progress.done();
        this.toastr.success('Data fetched successfully!');
        this.isBusy = false;

      }
    }, error => {
      console.log(error);
      this.toastr.error(error.message);
    });


  }

  updateVehicle(vehicle) {
    console.log(vehicle);
    const dialogRef = this.dialog.open(RegisterNewVehicleComponent, {
      width: '50%',
      data: { data: vehicle, isUpdate: true }
    });
  }

  deleteVehicle(vehicle) {
    const conf = confirm('Do you want to delete?');
    if (conf) {
      this.isBusy = true;
      this.service.deleteVehicleDetails(vehicle).subscribe((data: Response) => {
        if (data.Code !== 0) {
          alert(data.Message);
        } else {
          this.toastr.success(data.Message);
          this.getAllRegisteredVehicle();
        }
      }, error => {
        console.log(error);
        this.toastr.error(error.message);
        this.isBusy = false;

      });
    }
  }
}
