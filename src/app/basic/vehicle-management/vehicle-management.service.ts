import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HOME_URL } from 'src/app/_helper/appConstant';
import { Response } from 'src/app/models/Response';

@Injectable({
  providedIn: 'root'
})
export class VehicleManagementService {

  res: Observable<Response>;
  constructor(private http: HttpClient) { }

  registerNewVehicle(vehicle): Observable<Response> {
    return this.res = this.http.post<Response>(HOME_URL + 'vehicle/registervehicle', vehicle);
  }

  updateVehicleDetails(vehicle) {
    return this.res = this.http.post<Response>(HOME_URL + 'vehicle/updatevehicle', vehicle);

  }

  deleteVehicleDetails(vehicle) {
    return this.res = this.http.post<Response>(HOME_URL + 'vehicle/deletevehicle', vehicle);
  }

  getAllVehiclesDetails() {
    return this.res = this.http.get<Response>(HOME_URL + 'vehicle/getallvehicles');
  }

  reportIssue(issue) {
    return this.http.post<Response>(HOME_URL + 'vehicleissue/reportnewissue', issue);
  }

  getCustomerByConref(conref) {
    const data = {
      conRef: conref
    };
    return this.http.post<Response>(HOME_URL + 'customer/getcustomerbyid', data);
  }
}
