import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VehicleManagementService } from '../vehicle-management.service';
import { Response } from 'src/app/models/Response';
import { Vehicle } from 'src/app/models/Vehicle/vehicle';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
// import { NgProgress } from '@ngx-progressbar/core';


@Component({
  selector: 'app-register-new-vehicle',
  templateUrl: './register-new-vehicle.component.html',
  styleUrls: ['./register-new-vehicle.component.css'],
  providers: [VehicleManagementService]
})
export class RegisterNewVehicleComponent implements OnInit {

  isUpdate: boolean;
  vehicle = {} as Vehicle;
  customer = {} as any;

  constructor(
    public dialogRef: MatDialogRef<RegisterNewVehicleComponent>,
    private service: VehicleManagementService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
    // private progress: NgProgress,
  ) {
    this.vehicle = data.data;
    this.customer = data.data.customer;
    this.isUpdate = data.isUpdate;

    console.log(data);
  }

  foods: any[] = [
    { value: 'Body wash', viewValue: 'Body wash' },
    { value: 'Repair', viewValue: 'Repair' },
    { value: 'Paint', viewValue: 'Paint' }
  ];

  ngOnInit() {
    this.vehicle.addedDate = Date.now().toLocaleString();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  registerNewVehicle(form: NgForm) {

    const obj = {
      vehicle: this.vehicle,
      customer: this.customer
    };

    if (form.valid) {
      if (this.isUpdate) {
        // this.progress.start();
        this.service.updateVehicleDetails(obj).subscribe((data: Response) => {
          if (data != null) {
            this.toastr.success('Details updated successfully!');
            this.dialogRef.close();
          } else {
            this.toastr.error('Something went wroung. Please try again');
          }
        }, error => {
          this.toastr.error(error.message);
        });
      } else {
        // this.progress.start();
        this.service.registerNewVehicle(obj).subscribe((data: Response) => {
          if (data.Code === 0) {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          } else {
            this.toastr.error(data.Message);
          }
          // this.progress.done();
        }, error => {
          this.toastr.error(error.message);
        });

      }
    } else {
      this.toastr.warning('Please fill the mandetory fields!');
    }

  }

  getCustomerByConRef() {
    this.service.getCustomerByConref(this.customer.contactNumber).subscribe((data: Response) => {
      if (data.Code === 0) {
        this.customer = data.Result;
      } else {
        this.toastr.info('Customer not found! Please enter details');
      }
    });
  }

  searchByConref() {
    this.getCustomerByConRef();
  }

}
