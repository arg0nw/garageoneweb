import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { VehicleManagementService } from '../vehicle-management/vehicle-management.service';
import { ToastrService } from 'ngx-toastr';
import { CustomerManagementService } from './customer-management.service';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.css']
})
export class CustomerManagementComponent implements OnInit {

  customers = [];
  dataSource = new MatTableDataSource(this.customers);

  displayedColumns: string[] = [
    'customerName',
    'contactNumber',
    'action'
  ];

  constructor(
    public dialog: MatDialog,
    private service: CustomerManagementService,
    // private progress: NgProgress,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getAllCustomers();
  }

  getAllCustomers() {

  }
}
