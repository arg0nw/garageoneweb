import { Component, OnInit, Inject } from '@angular/core';
import { InventoryItem } from 'src/app/models/Inventory/inventoryItem';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegisterNewVehicleComponent } from '../../vehicle-management/register-new-vehicle/register-new-vehicle.component';
import { ToastrService } from 'ngx-toastr';
import { Customer } from 'src/app/models/Customer/Customer';
import { InventoryService } from '../inventory.service';
import { Response } from 'src/app/models/Response';

@Component({
  selector: 'app-add-new-item',
  templateUrl: './add-new-item.component.html',
  styleUrls: ['./add-new-item.component.css']
})
export class AddNewItemComponent implements OnInit {

  item = {} as InventoryItem;
  itemTypes = [];

  constructor(
    public dialogRef: MatDialogRef<RegisterNewVehicleComponent>,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private inventoryService: InventoryService
  ) {
    this.item = data.data;
    this.itemTypes = data.itemTypeList;
  }

  ngOnInit() {
  }

  addNewInventoryItem(form: NgForm) {
    if (form.valid) {
      if (!this.item.id) {
        this.inventoryService.addNewInventoryItem(this.item).subscribe((data: Response) => {
          if (data.Code !== 0) {
            this.toastr.error(data.Message);
          } else {
            this.toastr.success(data.Message);
            this.dialogRef.close();
          }
        }, error => {
          this.toastr.warning(error);
        });
      } else {
        if (this.item.id) {
          this.inventoryService.updateInventoryItem(this.item).subscribe((data: Response) => {
            if (data.Code !== 0) {
              this.toastr.error(data.Message);
            } else {
              this.toastr.success(data.Message);
              this.dialogRef.close();
            }
          }, error => {
            this.toastr.warning(error);
          });
        }
      }
    } else {
      this.toastr.warning('Please fill the mandetory fileds!');
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
