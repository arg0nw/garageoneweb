import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HOME_URL } from 'src/app/_helper/appConstant';
import { Response } from 'src/app/models/Response';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  res: Observable<Response>;
  constructor(private http: HttpClient) { }

  getAllInventoryItems() {
    return this.res = this.http.get<Response>(HOME_URL + 'inventory/getallitems');
  }

  addNewInventoryItem(item) {
    return this.res = this.http.post<Response>(HOME_URL + 'inventory/addnewitem', item);
  }

  updateInventoryItem(item) {
    return this.res = this.http.post<Response>(HOME_URL + 'inventory/updateitem', item);
  }

  deleteInventoryItem(item) {
    return this.res = this.http.post<Response>(HOME_URL + 'inventory/deleteitem', item);
  }

  updateItemUsage(usage) {
    return this.res = this.http.post<Response>(HOME_URL + 'inventory/usageupdate', usage);
  }

  getTypeList() {
    return this.res = this.http.get<Response>(HOME_URL + '');
  }
}
