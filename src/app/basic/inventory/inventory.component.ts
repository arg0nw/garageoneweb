import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { InventoryItem } from 'src/app/models/Inventory/inventoryItem';
import { AddNewItemComponent } from './add-new-item/add-new-item.component';
import { MessagepopComponent } from 'src/app/common/messagepop/messagepop.component';
import { InventoryService } from './inventory.service';
import { Response } from 'src/app/models/Response';
import { ToastrService } from 'ngx-toastr';
import { MastersService } from 'src/app/common/masters/masters.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  items = [];
  dataSource = new MatTableDataSource(this.items);
  itemTypeList = [];

  displayedColumns: string[] = [
    'itemCode',
    'itemName',
    'itemQuantity',
    'itemUnitPrice',
    'itemTypeId',
    'itemSellingPrice',
    'action'
  ];

  isBusy = false;

  constructor(
    public dialog: MatDialog,
    private service: InventoryService,
    private masterService: MastersService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getItemTypeList();
    this.getAllInventoryItemList();
  }

  openAddInventoryItem() {
    const item = {} as InventoryItem;
    const dialogRef = this.dialog.open(AddNewItemComponent, {
      width: '50%',
      data: { data: item, itemTypeList: this.itemTypeList }
    });
  }

  usageUpdate() {
    const dialogRef = this.dialog.open(MessagepopComponent, {
      width: '30%',
      data: { message: 'This feature will be available soon!' }
    });
  }

  openUpdateInventoryItem(item) {
    const dialogRef = this.dialog.open(AddNewItemComponent, {
      width: '50%',
      data: { data: item, itemTypeList: this.itemTypeList }
    });
  }


  viewItemUsageHistory() {
    const dialogRef = this.dialog.open(MessagepopComponent, {
      width: '30%',
      data: { message: 'This feature will be available soon!' }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getItemTypeList() {
    this.isBusy = true;
    this.masterService.getAllItemTypes().subscribe((data: Response) => {
      if (data.Code !== 0) {
        this.toastr.error(data.Message);
        this.isBusy = false;
      } else {
        this.itemTypeList = data.Result;
        this.isBusy = false;
      }
    }, error => {
      this.toastr.warning(error);
      this.isBusy = false;
    });
  }

  getAllInventoryItemList() {
    this.isBusy = true;
    this.service.getAllInventoryItems().subscribe((data: Response) => {
      if (data.Code === 0) {
        this.items = data.Result;
        this.dataSource = data.Result;
        this.toastr.success('Data fetched successfully!');
        this.isBusy = false;
      } else {
        this.toastr.error(data.Message);
        this.isBusy = false;
      }
    }, error => {
      this.toastr.warning(error);
      this.isBusy = false;
    });
  }

  deleteItem(item) {
    const conf = confirm('Do you want to delete?');
    if (conf) {
      this.isBusy = true;
      this.service.deleteInventoryItem(item).subscribe((data: Response) => {
        if (data.Code === 0) {
          this.toastr.success(data.Message);
          this.getAllInventoryItemList();
          // this.isBusy = false;
        } else {
          this.toastr.error(data.Message);
          this.isBusy = false;
        }
      }, error => {
        this.toastr.warning(error);
        this.isBusy = false;
      });
    }
  }
}
