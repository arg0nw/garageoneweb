import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { AddNewJobComponent } from './add-new-job/add-new-job.component';
import { JobServiceService } from './job-service.service';
import { Response } from 'src/app/models/Response';
import { UpdateJobComponent } from './update-job/update-job.component';
import { FinancialComponent } from 'src/app/pro/financial/financial.component';
import { InvoiceService } from 'src/app/pro/financial/invoice.service';
import { BilligServiceService } from 'src/app/services/billig-service.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-job-management',
  templateUrl: './job-management.component.html',
  styleUrls: ['./job-management.component.css']
})
export class JobManagementComponent implements OnInit {

  vehicles = [];
  dataSource = new MatTableDataSource(this.vehicles);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  Invoice = {} as any;

  isBusy = false;

  displayedColumns: string[] = [
    'taskId',
    // 'taskSubject',
    'vehiclePlateNumber',
    'taskEstimatedPrice',
    'action'
  ];

  constructor(
    public dialog: MatDialog,
    private toastr: ToastrService,
    private jobService: JobServiceService,
    private invoService: InvoiceService,
    private service: BilligServiceService,
    private datePipe: DatePipe,
  ) { }

  ngOnInit() {
    this.getAllJobs();
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllJobs() {
    this.isBusy = true;
    this.jobService.getAllJobs().subscribe((data: Response) => {
      if (data.Code === 0) {
        // this.items = data.Result;
        this.dataSource.data = data.Result;
        this.isBusy = false;
        this.toastr.success('Data fetched successfully!');
      } else {
        this.isBusy = false;
        this.toastr.error(data.Message);
      }
    }, error => {
      this.isBusy = false;
      this.toastr.warning(error);
    });
  }

  updateJobStatus(job) {
    const dialogRef = this.dialog.open(UpdateJobComponent, {
      width: '50%',
      data: { data: job }
    });
  }

  printInvoice(job) {

    this.isBusy = true;
    console.log(job);

    if (job.taskId) {
      this.service.getJobdataById(job.taskId).subscribe((res: any) => {
        console.log(res);
        res.Result.mainData.createdAt = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.Invoice = res.Result;
        this.isBusy = false;

        this.Invoice.mainData.printType = !this.Invoice.mainData.isEstimation ? 'Sales Invoice' : 'Estimation';
        this.Invoice.mainData.InvoiceOrEstimationNo = !this.Invoice.mainData.isEstimation ? 'Invoice No' : 'Estimation No';
        this.Invoice.mainData.taskServiceCharge = (Number(!this.Invoice.mainData.taskServiceCharge ? 0.00 :
          this.Invoice.mainData.taskServiceCharge).toFixed(2));
        this.Invoice.mainData.totalInvoiceAmount = !this.Invoice.mainData.totalInvoiceAmount ?
          this.Invoice.mainData.taskEstimatedPrice :
          Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
        this.Invoice.mainData.discount = !this.Invoice.mainData.discount ? '0' : Number(this.Invoice.mainData.discount);

        this.Invoice.mainData.totalInvoiceAmount = Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
        this.Invoice.mainData.taskEstimatedPrice = Number(this.Invoice.mainData.taskEstimatedPrice).toFixed(2);

        const obj = {
          discountAmount: this.Invoice.mainData.discount,
          invoiceTotal: this.Invoice.mainData.totalInvoiceAmount,
          meterReading: this.Invoice.vehicle.meterReading,
          nextService: this.Invoice.vehicle.nextService,
          serviceSpecificTaskId: this.Invoice.mainData.id
        };

        if (this.Invoice.mainData.isEstimation) {
          this.Invoice.mainData.invoiceNumber = this.Invoice.mainData.taskId;
          this.service.getAllJobsReport(this.Invoice);
          this.isBusy = false;
        } else {
          this.invoService.generateInvoiceNumber(obj).subscribe(data => {
            console.log(data);
            this.Invoice.mainData.invoiceNumber = data.Result.invoiceNumber;
            this.service.getAllJobsReport(this.Invoice);
            this.isBusy = false;
          }, InvoErr => {
            this.toastr.warning('Invoice generating faild!. Please try again later');
            console.log(InvoErr);
          });
        }
      },
        seErr => {
          this.toastr.warning('Something went wroung. Please try again later');
          console.log(seErr);
        });
    }
  }

}
