import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-update-job',
  templateUrl: './update-job.component.html',
  styleUrls: ['./update-job.component.css']
})
export class UpdateJobComponent implements OnInit {

  job = {} as any;
  items = [];

  statusList = [
    {id: 1, name: 'Pending'},
    {id: 2, name: 'In Progress'},
    {id: 3, name: 'Ready to Review'},
    {id: 4, name: 'Done'},
  ];

  constructor(
    public dialogRef: MatDialogRef<UpdateJobComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.job = data.data;
    this.items = this.job.taskRelatedItems;
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
