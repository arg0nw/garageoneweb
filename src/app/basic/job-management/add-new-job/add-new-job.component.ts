import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Vehicle } from 'src/app/models/Vehicle/vehicle';
import { SearchJobComponent } from '../search-job/search-job.component';
import { InventoryService } from '../../inventory/inventory.service';
import { Response } from 'src/app/models/Response';
import { JobServiceService } from '../job-service.service';
import { Job } from 'src/app/models/Job/Job';
import { NgForm } from '@angular/forms';
import { BilligServiceService } from 'src/app/services/billig-service.service';
import { MastersService } from 'src/app/common/masters/masters.service';
import { Observable } from 'rxjs';
import { VehicleManagementService } from '../../vehicle-management/vehicle-management.service';
import { map, startWith } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { InvoiceService } from 'src/app/pro/financial/invoice.service';


@Component({
  selector: 'app-add-new-job',
  templateUrl: './add-new-job.component.html',
  styleUrls: ['./add-new-job.component.css']
})
export class AddNewJobComponent implements OnInit {
  items = [];
  services = [];

  vehiclePlateNumbers = [];
  vehicleList = [];

  InvoiceEstimationType = '1';

  Invoice = {} as any;


  displayedColumns: string[] = [
    'select',
    'itemCode',
    'itemName',
    'itemQuantity',
    'itemTypeId',
    'itemSellingPrice'
  ];

  statusList = [
    { id: 1, name: 'Pending' },
    { id: 2, name: 'In Progress' },
    { id: 3, name: 'Ready to Review' },
    { id: 4, name: 'Done' },
  ];

  isBusy = false;
  dataSource = new MatTableDataSource(this.items);
  selection = new SelectionModel(true, []);
  selectedItemCodes = [];
  replacePartAmount = '0.00';
  serviceCharge = '';
  estimatedPrice = '0.00';

  job = {} as Job;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private service: InventoryService,
    private toastr: ToastrService,
    private jobService: JobServiceService,
    private billservice: BilligServiceService,
    private masterService: MastersService,
    private vehicleService: VehicleManagementService,
    private datePipe: DatePipe,
    private invoService: InvoiceService,
  ) { }

  applyFilter(filterValue: string, $event) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.isBusy = true;
    this.getAllInventoryItemList();
    this.dataSource.paginator = this.paginator;
    this.getServiceTypes();

    this.vehicleService.getAllVehiclesDetails().subscribe(data => {
      // console.log(data);
      data.Result.forEach(element => {

        if (element && element.plateNumber) {
          this.vehicleList.push(element.plateNumber);
        }
      });

      this.vehiclePlateNumbers = this.vehicleList;
      this.isBusy = false;
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  checkBoxSelected($event, row: any) {

    this.selection.toggle($event);
    if ($event.checked) {
      this.replacePartAmount = (
        Number(this.replacePartAmount) + row.itemSellingPrice
      ).toFixed(2);
      this.selectedItemCodes.push(row.id);
      console.log(this.selectedItemCodes);
    } else {
      this.replacePartAmount = (
        Number(this.replacePartAmount) - row.itemSellingPrice
      ).toFixed(2);
      const index = this.selectedItemCodes.indexOf(row.itemCode, 0);

      this.selectedItemCodes.splice(index, 1);

    }

    this.calculateTotal();
  }

  calculateTotal() {
    this.estimatedPrice = (
      Number(this.serviceCharge) + Number(this.replacePartAmount)
    ).toFixed(2);
  }

  searchByVehiclePlateNumber() {
    const vehicle = {} as Vehicle;
    const dialogRef = this.dialog.open(SearchJobComponent, {
      width: '50%',
      data: { data: vehicle, isUpdate: false }
    });
  }

  getAllInventoryItemList() {
    // this.isBusy = true;
    this.service.getAllInventoryItems().subscribe(
      (data: Response) => {
        if (data.Code === 0) {
          this.items = data.Result;
          this.dataSource.data = data.Result;
          // this.isBusy = false;

        } else {
          this.toastr.error(data.Message);
          // this.isBusy = false;

        }
      },
      error => {
        this.toastr.warning(error);
        // this.isBusy = false;
      }
    );
  }

  saveJob(form: NgForm) {
    if (form.valid) {
      this.postjob();
    } else {
      this.toastr.warning('Please fill mandetory fields');
    }
  }

  getbill() {
    this.billservice.getJobdataById(1);
  }

  getServiceTypes() {
    this.masterService.getAllServiceRepairType().subscribe((data: Response) => {
      if (data.Code === 0) {
        this.services = data.Result;
        this.isBusy = false;
      } else {
        this.toastr.error(data.Message);
        this.isBusy = false;
      }
    });

  }

  addServiceCharge() {
    const val = this.services.find(obj => {
      if (obj.id === this.job.serviceTypeId) {
        return obj.unitPrice;
      }
    });

    this.serviceCharge = val.unitPrice ? Number(val.unitPrice).toFixed(2) : '0.00';
    this.calculateTotal();

  }

  public filter(myform: NgForm) {
    const filterValue = myform.control.controls.VehiclePlateNumber.value.toLowerCase();
    this.vehiclePlateNumbers = this.vehicleList.filter(option => option.toLowerCase().includes(filterValue));
  }

  getPrints() {
    this.isBusy = true;
    if (this.job.taskId) {
      this.printIt();
    } else {
      this.isBusy = true;
      this.job.taskEstimatedPrice = this.estimatedPrice;
      this.job.taskItemsTotalAmount = this.replacePartAmount;
      this.job.taskServiceCharge = this.serviceCharge;
      this.job.itemList = this.selectedItemCodes;
      this.job.taskAssignTo = 1;
      this.job.isEstimation = this.InvoiceEstimationType === '1' ? false : true;
      this.jobService.postNewJob(this.job).subscribe((data: Response) => {
        console.log(data);
        this.job = data.Result[0];
        this.printIt();
        this.isBusy = false;
      });
    }
  }

  postjob() {
    this.isBusy = true;
    this.job.taskEstimatedPrice = this.estimatedPrice;
    this.job.taskItemsTotalAmount = this.replacePartAmount;
    this.job.taskServiceCharge = this.serviceCharge;
    this.job.itemList = this.selectedItemCodes;
    this.job.taskAssignTo = 1;
    this.job.isEstimation = this.InvoiceEstimationType === '1' ? false : true;
    this.jobService.postNewJob(this.job).subscribe((data: Response) => {
      console.log(data);
      this.job = data.Result[0];
      this.isBusy = false;
    });
  }

  printIt() {
    if (this.job.taskId) {
      this.billservice.getJobdataById(this.job.taskId).subscribe((res: any) => {
        console.log(res);
        res.Result.mainData.createdAt = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.Invoice = res.Result;
        this.isBusy = false;

        this.Invoice.mainData.printType = !this.Invoice.mainData.isEstimation ? 'Sales Invoice' : 'Estimation';
        this.Invoice.mainData.InvoiceOrEstimationNo = !this.Invoice.mainData.isEstimation ? 'Invoice No' : 'Estimation No';
        this.Invoice.mainData.taskServiceCharge = (Number(!this.Invoice.mainData.taskServiceCharge ? 0.00 :
          this.Invoice.mainData.taskServiceCharge).toFixed(2));
        this.Invoice.mainData.totalInvoiceAmount = !this.Invoice.mainData.totalInvoiceAmount ?
          this.Invoice.mainData.taskEstimatedPrice :
          Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
        this.Invoice.mainData.discount = !this.Invoice.mainData.discount ? '0' : Number(this.Invoice.mainData.discount);

        this.Invoice.mainData.totalInvoiceAmount = Number(this.Invoice.mainData.totalInvoiceAmount).toFixed(2);
        this.Invoice.mainData.taskEstimatedPrice = Number(this.Invoice.mainData.taskEstimatedPrice).toFixed(2);

        const obj = {
          discountAmount: this.Invoice.mainData.discount,
          invoiceTotal: this.Invoice.mainData.totalInvoiceAmount,
          meterReading: this.Invoice.vehicle.meterReading,
          nextService: this.Invoice.vehicle.nextService,
          serviceSpecificTaskId: this.Invoice.mainData.id
        };

        if (this.Invoice.mainData.isEstimation) {
          this.Invoice.mainData.invoiceNumber = this.Invoice.mainData.taskId;
          this.billservice.getAllJobsReport(this.Invoice);
          this.isBusy = false;
        } else {
          this.invoService.generateInvoiceNumber(obj).subscribe(data => {
            console.log(data);
            this.Invoice.mainData.invoiceNumber = data.Result.invoiceNumber;
            this.billservice.getAllJobsReport(this.Invoice);
            this.isBusy = false;
          }, InvoErr => {
            this.toastr.warning('Invoice generating faild!. Please try again later');
            console.log(InvoErr);
          });
        }
      },
        seErr => {
          this.toastr.warning('Something went wroung. Please try again later');
          console.log(seErr);
        });
    }
  }

  Clear(myform: NgForm) {
    myform.reset();
  }
}
