import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HOME_URL } from 'src/app/_helper/appConstant';
import { Response } from 'src/app/models/Response';

@Injectable({
  providedIn: 'root'
})
export class JobServiceService {
  res: Observable<Response>;

  constructor(private http: HttpClient) { }

  postNewJob(obj) {
    return this.http.post<Response>(HOME_URL + 'serviceTask/postnewserviceestimation', obj);
  }

  getAllJobs() {
    return this.http.get<Response>(HOME_URL + 'serviceTask/getallserviceestimations');
  }

  getJobDataById(jobId) {
    return this.http.get<Response>(HOME_URL + 'serviceTask/' + jobId);
  }
}
